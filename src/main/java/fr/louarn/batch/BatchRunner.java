package fr.louarn.batch;

import fr.louarn.batch.service.IBatchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

import static java.lang.System.exit;

@Slf4j
public class BatchRunner {

    /**
     * Porivate constructe.
     */
    private BatchRunner() {
    }

    /**
     * LINE_START.
     */
    private static final String LINE_START = "\n--------------------------\n";

    /**
     * LINE_END.
     */
    private static final String LINE_END = "--------------------------";

    /**
     * OK.
     */
    private static final String OK = "----------- OK -----------\n";

    /**
     * KO.
     */
    private static final String KO = "----------- KO -----------\n";

    /**
     * Méthode de lancement du batch.
     *
     * @param log   log
     * @param clazz clazz
     * @param args  args
     * @param <T>   clazz
     */
    public static <T> void run(Class<T> clazz, String... args) {

        if (args.length != 1) {
            log.error("Selectionner le nom du batch à exécuter en argument");
            exit(1);
        }

        // Chargement du context
        ApplicationContext applicationContext = SpringApplication.run(clazz);

        // Récupération du service
        IBatchService batchService = applicationContext.getBean(args[0], IBatchService.class);

        StringBuilder builder = new StringBuilder(LINE_START);
        if (batchService.run(args)) {
            builder.append(OK);
        } else {
            builder.append(KO);
        }
        builder.append(LINE_END);
        log.info("{}", builder);
    }

}
