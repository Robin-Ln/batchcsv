package fr.louarn.batch.dao.impl;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectReader;
import fr.louarn.batch.dao.ICsvDao;
import fr.louarn.batch.modele.CsvDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

@Repository
public class CsvDaoImpl implements ICsvDao {

    /**
     * ObjectReader.
     */
    private ObjectReader objectReader;

    /**
     * Constructeur.
     *
     * @param objectReader objectReader
     */
    @Autowired
    public CsvDaoImpl(ObjectReader objectReader) {
        this.objectReader = objectReader;
    }

    @Override
    public void readLine(Consumer<CsvDto> csvConsumer, String pathName) throws IOException {

        if (pathName == null || "".equals(pathName)) {
            throw new IOException("Selectionner un fichier valide");
        }

        File file = new File(pathName);
        MappingIterator<CsvDto> it = objectReader.readValues(file);
        while (it.hasNext()) {
            CsvDto csvDto = it.next();
            csvConsumer.accept(csvDto);
        }
    }
}
