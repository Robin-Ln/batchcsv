package fr.louarn.batch.dao;

import fr.louarn.batch.modele.CsvDto;

import java.io.IOException;
import java.util.function.Consumer;

public interface ICsvDao {

    /**
     * readLine.
     *
     * @param csvConsumer csvConsumer
     * @param pathName    pathName
     * @throws IOException
     */
    void readLine(Consumer<CsvDto> csvConsumer, String pathName) throws IOException;

}
