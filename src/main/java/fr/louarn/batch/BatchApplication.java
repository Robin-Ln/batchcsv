package fr.louarn.batch;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchApplication {

    /**
     * Main.
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        BatchRunner.run(BatchApplication.class, args);
    }
}
