package fr.louarn.batch.service.impl;

import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import fr.louarn.batch.config.CsvProperties;
import fr.louarn.batch.consumer.CsvConsumer;
import fr.louarn.batch.dao.ICsvDao;
import fr.louarn.batch.service.IBatchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service("csvService")
public class CsvServiceImpl implements IBatchService {

    /**
     * CsvConsumer.
     */
    private CsvConsumer csvConsumer;


    /**
     * CsvProperties.
     */
    private CsvProperties csvProperties;

    /**
     * ObjectReader.
     */
    private ICsvDao csvDao;

    /**
     * Constructeur.
     *
     * @param csvConsumer   CsvConsumer
     * @param csvProperties csvProperties
     * @param csvDao        csvDao
     */
    @Autowired
    public CsvServiceImpl(CsvConsumer csvConsumer,
                          CsvProperties csvProperties,
                          ICsvDao csvDao) {
        this.csvConsumer = csvConsumer;
        this.csvProperties = csvProperties;
        this.csvDao = csvDao;
    }

    @Override
    public boolean run(String... args) {
        try {
            csvDao.readLine(csvConsumer, csvProperties.getCsvFile());
        } catch (IOException | RuntimeJsonMappingException e) {
            log.info("Erreur lors de la lecture du fichier csv", e);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
