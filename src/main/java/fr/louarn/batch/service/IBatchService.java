package fr.louarn.batch.service;

public interface IBatchService {

    /**
     * run.
     *
     * @param args args
     * @return boolean
     */
    boolean run(String... args);

}
