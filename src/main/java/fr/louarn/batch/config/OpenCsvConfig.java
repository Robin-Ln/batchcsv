package fr.louarn.batch.config;

import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Configuration
public class OpenCsvConfig {


    public <T> void writeCsv(String pathFile, List<T> elements) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

        // Création du répertoire parent du fichier
        if (!createParentDir(pathFile)) {
            return;
        }

        FileWriter writer = new FileWriter(pathFile);

        StatefulBeanToCsvBuilder<T> builder = new StatefulBeanToCsvBuilder<>(writer);

        StatefulBeanToCsv<T> statefulBeanToCsv = builder.withSeparator(';').build();

        for (T element : elements) {
            statefulBeanToCsv.write(element);
        }

        writer.close();
    }

    private boolean createParentDir(String pathFile) {
        File file = new File(pathFile);

        if (file.exists()) {
            return true;
        }

        File directory = file.getParentFile();

        if (directory.exists()) {
            return true;
        }

        return directory.mkdirs();
    }

}
