package fr.louarn.batch.consumer;

import fr.louarn.batch.modele.CsvDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Slf4j
@Component
public class CsvConsumer implements Consumer<CsvDto> {

    @Override
    public void accept(CsvDto csvDto) {
        log.info(csvDto.toString());
    }
}
