package fr.louarn.batch.modele;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.opencsv.bean.CsvBindByPosition;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CsvDto {

    /**
     * header 1.
     */
    @CsvBindByPosition(position = 0)
    @JsonPropertyOrder(value = "header1")
    private String header1;

    /**
     * header 2.
     */
    @CsvBindByPosition(position = 1)
    @JsonPropertyOrder(value = "header2")
    private String header2;

    /**
     * header 3.
     */
    @CsvBindByPosition(position = 2)
    @JsonPropertyOrder(value = "header3")
    private String header3;
}
