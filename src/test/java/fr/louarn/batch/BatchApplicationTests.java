package fr.louarn.batch;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import fr.louarn.batch.config.OpenCsvConfig;
import fr.louarn.batch.modele.CsvDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Collections;

@SpringBootTest
class BatchApplicationTests {

    @Autowired
    OpenCsvConfig openCsvConfig;

    @Test
    void contextLoads() throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        openCsvConfig.writeCsv("logs/test.csv",
                Collections.singletonList(
                        CsvDto.builder()
                                .header1("asdq1s")
                                .header2("qsdqsd")
                                .header3("qsdqsdsq")
                                .build()
                )
        );

    }

}
